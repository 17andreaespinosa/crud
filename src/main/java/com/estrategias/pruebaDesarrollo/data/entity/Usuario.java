package com.estrategias.pruebaDesarrollo.data.entity;

import com.estrategias.pruebaDesarrollo.data.enums.Estado;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "cedula_usuario", nullable = false, length = 12)
    private String CedulaUsuario;

    @Column(name = "primer_nombre", nullable = false, length = 50)
    private String primerNombre;

    @Column(name = "segundo_nombre", nullable = false, length = 50)
    private String segundoNombre;

    @Column(name = "primer_apellido", nullable = false, length = 45)
    private String primerApellido;

    @Column(name = "segundo_apellido", nullable = false, length = 45)
    private String segundoApellido;

    @Column(nullable = false, length = 150)
    private String clave;

    @Column(nullable = false, length = 50)
    private String email;

    @Column(name = "nit_empresa", nullable = false, length = 12)
    private String nitEmpresa;

    private Estado estado;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", insertable = false, updatable = false,referencedColumnName = "id")
    @JsonIgnore
    private Empresa empresa;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name="rol_id", insertable = false, updatable = false, referencedColumnName = "id")
    @JsonIgnore
    private Rol rol;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCedulaUsuario() {
        return CedulaUsuario;
    }

    public void setCedulaUsuario(String cedulaUsuario) {
        CedulaUsuario = cedulaUsuario;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }
}
