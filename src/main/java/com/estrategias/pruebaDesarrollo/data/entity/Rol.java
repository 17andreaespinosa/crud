package com.estrategias.pruebaDesarrollo.data.entity;

import com.estrategias.pruebaDesarrollo.data.enums.Estado;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.omg.PortableInterceptor.INACTIVE;

import javax.persistence.*;
import java.util.Set;

//
@Entity
@Table(name = "roles")
public class Rol {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, length = 50)
    private String rol;

    private Estado estado;

    @OneToMany
    @JoinColumn(name = "rol_id")
    @JsonIgnore
    private Set<Usuario> usuarios;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
