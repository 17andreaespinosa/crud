package com.estrategias.pruebaDesarrollo.data.enums.convertEnum;
import com.estrategias.pruebaDesarrollo.data.enums.Estado;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter(autoApply = true)
public class ConvertEstado implements AttributeConverter<Estado, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Estado estado) {
        if (estado == null) {
            return null;
        }
        return estado.getCode();
    }

    @Override
    public Estado convertToEntityAttribute(Integer code) {
        if (code == null) {
            return null;
        }

        return Stream.of(Estado.values())
                .filter(c -> c.getCode() == code)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
