package com.estrategias.pruebaDesarrollo.data.enums;

import java.util.HashMap;
import java.util.Map;

public enum Estado {

    ACTIVO(1), INACTIVO(0);

    private Integer code;

    private Estado(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
